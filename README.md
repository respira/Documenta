![Build Status](https://gitlab.com/respira/Documenta/badges/master/pipeline.svg)

## Acesso à documentação 

---
 https://respira.gitlab.io/Documenta/

---



## GitLab CI


A página estática HTML da documentação é criada automaticamente após cada push no master via [GitLab CI][ci], 
de acordo com o descrito em  [`.gitlab-ci.yml`](.gitlab-ci.yml). 

As configurações para o [sphinx] (temas, extensões etc.) são definidas no arquivo [`.conf.py`](.conf.py).




## Requisitos

- [Sphinx][]

## Gerando localmente

Caso queira gerar os documentos localmente, faça o seguinte :

1. Faça o fork, clone or download desse projeto
1. [Installe][sphinx] Sphinx
1. Para gerar a documentação: `make`

Além do HTML, é possível gerar o PDF a partir do Latex. Para isso faça ..... 

A localização do HTML produzido é definida em [`.conf.py`](.conf.py), neste caso `_build/html`.

---

1. A documentação do projeto Respira foi preparada usando [sphinx] e publicada no gitab pages. 
Para mais informações acesse  https://docs.gitlab.com/ee/user/project/pages/.

2. Esse repositório foi *forked* inicialmente de  https://gitlab.com/pages/sphinx .

[ci]: https://about.gitlab.com/product/continuous-integration/
[userpages]: https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html#user-and-group-website-examples
[projpages]: https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html#project-website-examples
[sphinx]: http://www.sphinx-doc.org/

