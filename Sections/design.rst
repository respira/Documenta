.. _design:

Projeto
=======

.. toctree::
   :maxdepth: 2


Figuras
-------

.. figure:: /Fig/covid_detector_assembly.jpg
   :scale: 20%
   :align: center

.. figure:: /Fig/detector_box_base_cooler-angled_view.jpg
   :scale: 20%
   :align: center

.. figure:: /Fig/electronic_box_angled_view.jpg
   :scale: 20%
   :align: center

.. figure:: /Fig/electronic_box_top_view.jpg
   :scale: 20%
   :align: center


