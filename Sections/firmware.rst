.. _firmware:

Firmware
========

.. toctree::
   :maxdepth: 2

Diagrama de estados
-------------------

.. warning:: Isso não é um diagaram de estado !

.. mermaid::

   graph TD;
      A-->B;
      A-->C;
      B-->D;
      C-->D;
