.. _results:

Resultados
==========

.. toctree::
   :maxdepth: 2

Resultados - ciclo completo
---------------------------

.. figure:: /Fig/power_log.pdf
   :alt: Tensões e correntes durante a operação
   :scale: 100%
   :align: center

.. figure:: /Fig/conditions_log.pdf
   :scale: 100%
   :align: center

.. figure:: /Fig/dust_log.pdf
   :scale: 100%
   :align: center
