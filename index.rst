Documentação do protótipo do projeto Respira 
============================================

Escrever uma pequena introdução aqui , falando dos **ojetivos** e *histórico* desse projeto


Repositório
-----------

Toda a informação referente a esse projeto pode ser encontrada nos seguintes repositórios :

* https://gitlab.com/respira/firmware

* https://gitlab.com/respira/hardware

.. warning::

   Essa documentação está incompleta

.. danger::

   Use a máscara !!

.. admonition:: Relaxa !

   Já disse pra você relaxar !

.. caution::

   Sai corona !!

.. toctree::
   :maxdepth: 3

   O Projeto            </Sections/design>
   A Eletrônica         </Sections/electronics>
   O Firmware           </Sections/firmware>
   Aquisição de dados   </Sections/reader>
   Os Resultados        </Sections/results>
   Referências          </Sections/references>
